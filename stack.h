#ifndef STACK_H
#define STACK_H

#define MAX_STACK_SIZE 50
class stack {
public:
	stack();
	~stack();

	void push(char element);
	char pop();
	char Top();
	bool isEmpty();
	char elements[MAX_STACK_SIZE];
	int top;
};

#endif
